// Отримати число від користувача
const userInput = prompt("Введіть число:");

// Перетворити введений рядок на число
const number = parseInt(userInput);

// Перевірити, чи введене значення є числом
if (isNaN(number)) {
  console.log("Введено некоректне число");
} else {
  let foundNumbers = [];

  // Знайти числа, кратні 5, від 0 до введеного числа
  for (let i = 0; i <= number; i++) {
    if (i % 5 === 0) {
      foundNumbers.push(i);
    }
  }

  // Вивести знайдені числа або повідомлення, якщо немає чисел
  if (foundNumbers.length > 0) {
    console.log("Числа, кратні 5:");
    console.log(foundNumbers);
  } else {
    console.log("Sorry, no numbers");
  }
}
